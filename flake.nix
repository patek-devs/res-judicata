{
  description = "Res judicata";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-20.09";

  outputs = { self, nixpkgs }:
  let
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
  in
   {

      packages.x86_64-linux.form-dev = pkgs.mkShell {
        buildInputs = [ pkgs.nodejs-14_x ];
        shellHook = ''
        '';
      };

      packages.x86_64-linux.validator-dev =
      let
        geneea_sdk = pkgs.python38Packages.buildPythonPackage rec {
          pname = "geneea-nlp-client";
          version = "1.2.0";
          src = pkgs.python38Packages.fetchPypi {
            inherit pname version;
            sha256 = "Q0fYD3V0NbUOItpCwA6ExIy7sIcQxpfqr1aPSXd4+cc=";
          };
          propagatedBuildInputs = with pkgs.python38Packages; [ requests retrying ];
          doCheck = false;
        };
        docx2txt = pkgs.python38Packages.buildPythonPackage rec {
          pname = "docx2txt";
          version = "0.8";
          src = pkgs.python38Packages.fetchPypi {
            inherit pname version;
            sha256 = "LAbZjXz+LTlH5XYKV9kk4/8HdFs3nIc3cjki5wCSNuU=";
          };
        };
      in
        pkgs.mkShell {
          buildInputs = with pkgs.python38Packages; [ pkgs.python38 pip geneea_sdk pytesseract pdftotext pillow docx2txt flask flask-cors regex pdf2image ];
          shellHook = ''
            read -p 'Insert Geneea API key:' -r -s geneea_api_key
            export geneea_api_key
          '';
        };

    };
}
