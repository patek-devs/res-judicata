from flask import Flask, request, jsonify, send_file
from flask_cors import CORS
import validator
from docx2pdf import convert

app = Flask(__name__)
CORS(app)


@app.route('/validator', methods=['POST'])
def validate():
    if request.method == 'POST':
        raw_file = request.files['file']
        text_content = validator.process_file(raw_file)
        validation_result = validator.validate(text_content)
        return jsonify(validation_result)

@app.route('/to_pdf', methods=['POST'])
def convert_to_pdf():
    if request.method == 'POST':
        raw_file = request.files['file']
        _, ext = os.path.splitext(f.filename)
        if ext == ".docx":
            return send_file(convert(raw_file))
        elif ext == ".pdf":
            return send_file(raw_file)
        else:
            return "Bad file format", 400


app.run()
