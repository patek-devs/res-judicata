#!/usr/bin/env python

import os
from geneeanlpclient import g3


def getKey() -> str:
    key = os.getenv("geneea_api_key")
    if key is None:
        raise ValueError("$geneea_api_key env var was not set")
    return key


def doTheTest():
    builder = g3.Request.Builder(analyses=[g3.AnalysisType.ALL], returnMentions=True, returnItemSentiment=True, domain=g3.Domain.MEDIA, textType="Business", referenceDate="2015-03-14", diacritization="redo", language="cs")
    with g3.Client.create(userKey=getKey()) as analyzer:
        result = analyzer.analyze(
            builder.build(id=str(1), text='Koláč s rozinkami panu Karlovi moc chutnal.', language="cs"))

        # print(result)

        # for t in result.tokens:
        #     print(f'{t} – lemma "{t.lemma}"')

        for r in result.relations:
            # print(r)
            print(f'{r.type} {r.textRepr} - {r.name}')


if __name__ == '__main__':
    doTheTest()
