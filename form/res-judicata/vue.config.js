module.exports = {
    css: {
      loaderOptions: {
      }
    },
    devServer: {
      disableHostCheck: true
    },
    configureWebpack: {
        devtool: 'source-map'
    }
  }
